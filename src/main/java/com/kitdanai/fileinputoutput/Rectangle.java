/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kitdanai.fileinputoutput;

import java.io.Serializable;

/**
 *
 * @author ADMIN
 */
public class Rectangle implements Serializable{
    private int witdth;
    private int height;

    public Rectangle(int witdth, int height) {
        this.witdth = witdth;
        this.height = height;
    }

    public int getWitdth() {
        return witdth;
    }

    public void setWitdth(int witdth) {
        this.witdth = witdth;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    @Override
    public String toString() {
        return "Rectangle{" + "witdth=" + witdth + ", height=" + height + '}';
    }
    
    
}
